# The C++ Programming Language
El libro *The C++ Programming Language* es la referencia por excelente para aprender a programar en C++. El autor es el creador del lenguaje, *Bjarne Stroustrup*. Es un libro muy extenso que puede utilizarse tanto como referencia como guía paso a paso. La idea de detrás de C++ era seguir siendo un lenguaje de bajo nivel, como C, y agregar características que facilitaran la solución de problemas. 

C++ es un lenguaje multipropósito extensible para resolver cualquier tipo de tareas. Permite programar siguiendo diferentes técnicas:

- Programación procedural
- Abstracción de datos
- Programación orientada a objetos
- Programación genérica

No es tan importante conocer cada detalle a fondo del lenguaje como entender los conceptos fundamentales que lo forman, e.g. *sistema de tipado*, *manejo de memoria*, *objetos*, entre otros. Tampoco basta con conocerlos, parafraseando a Stroustrup:

> La pregunta, ¿cómo escribir *buenos* programas en C++?, es parecida a ¿cómo escribir bien? Se necesita saber qué quiere decir uno, practicar e imitiar buenos ejemplos. Con el tiempo vendrá la respuesta sola.

Seguiré su consejo y utilizaré este repositorio como mi cuaderno de apuntes, ejercicios e ideas que me vayan surgiendo mientras avance en el libro.


## Consejos
Los siguientes consejos son parafraseados del libro. No están escritos en piedra, pero son una buena guía para empezar.

1. Representar ideas o conceptos en código como, por ejemplo, una función, clase o enumeración.
2. Haz código elegante y eficiente.
3. No abuses de la abstracción.
4. Utiliza bibliotecas para diseñar tus soluciones y cuando puedas utiliza la biblioteca estándar
5. Utiliza relaciones entre abstracciones, por ejemplo, con una jerarquía de clases o parametrización.
6. Las ideas independientes también deben serlo en código. Evita dependencias entre clases.
7. C++ no solo es orientado a objetos.
8. C++ no solo es para programación genérica.
9. El sistema de tipado estático existe por algo, utilízalo.
10. Haz los recursos explícitos, representándolos como objetos de una clase.
12. El código de bajo nivel no siempre significa que sea más eficiente. No tengas miedo de usar componentes de alto nivel cuando se requiera.
13. Si un objeto/data tiene un invariante, encapsúlalo. Es decir, checa las condiciones que deben ser verdaderas siempre (invariantes) para que su correcto funcionamiento[^1].
14. ¡Practica, practica y practica! No sólo sigas un libro o tutoriales. Haz un proyecto que te interese, así aprenderás más.


[^1]: Importantísimo. Evitará que tengas errores a la larga.